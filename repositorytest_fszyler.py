# -*- coding: utf-8 -*-

import repository_fszyler
import sqlite3
import unittest

db_path = 'trading_accounts.db'

class RepositoryTest(unittest.TestCase):

	def setUp(self):
		conn = sqlite3.connect(db_path)
		c = conn.cursor()
		c.execute('DELETE FROM stocks')
		c.execute('DELETE FROM trading_accounts')
		c.execute('''INSERT INTO trading_accounts VALUES (1, '2005-02-02', 'active', NULL, 12475)''')
		c.execute('''INSERT INTO trading_accounts VALUES (2, '2006-04-02', 'active', NULL, 48852.12)''')
		c.execute('''INSERT INTO trading_accounts VALUES (3, '2006-05-24', 'active', NULL, 510)''')
		c.execute('''INSERT INTO stocks VALUES (1, 'ACW:US', 500, 12.45)''')
		c.execute('''INSERT INTO stocks VALUES (3, 'GSK:LN', 340, 1.5)''')
		c.execute('''INSERT INTO stocks VALUES (1, 'D5I:GR', 50, 125)''')
		c.execute('''INSERT INTO stocks VALUES (1, 'STAN:LN', 2340, 20.63)''')
		c.execute('''INSERT INTO stocks VALUES (2, 'ACW:US', 32, 18.06)''')
		conn.commit()
		conn.close()

	def tearDown(self):
		conn = sqlite3.connect(db_path)
		c = conn.cursor()
		c.execute('DELETE FROM stocks')
		c.execute('DELETE FROM trading_accounts')
		conn.commit()
		conn.close()

	def testGetByIdInstance(self):
		trading_account = repository_fszyler.TradingAccountRepository().getById(2)
		self.assertIsInstance(trading_account, repository_fszyler.TradingAccount, "Obiekt nie jest klasy TradingAccount")		

	def testGetByIdNotFound(self):
		self.assertEqual(repository_fszyler.TradingAccountRepository().getById(4),
				None, "Powinno wyjść None")

	def testGetByIdInvitemsLen(self):
		self.assertEqual(len(repository_fszyler.TradingAccountRepository().getById(1).stocks),
				3, "Powinno wyjść 3")
			
	def testDeleteNotFound(self):
		self.assertRaises(repository_fszyler.RepositoryException,
				repository_fszyler.TradingAccountRepository().delete, 22)
	
if __name__ == "__main__":
	unittest.main()
