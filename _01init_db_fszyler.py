# -*- coding: utf-8 -*-

import sqlite3


db_path = 'trading_accounts.db'
conn = sqlite3.connect(db_path)

c = conn.cursor()
#
# Tabele dostosowane do sqllite3
#
c.execute('''
          CREATE TABLE trading_accounts (
           id_trading_accounts INTEGER,
           opening_date NUMERIC NOT NULL,
           active TEXT NOT NULL,
           closing_date NUMERIC NULL,
		   balance REAL NOT NULL,
           PRIMARY KEY(id_trading_accounts)
          )
          ''')
c.execute('''
          CREATE TABLE stocks (
			trading_accounts_id INTEGER NOT NULL,
			stocks_ticker TEXT,
			stocks_qty INTEGER,
			buying_price REAL NOT NULL,
			FOREIGN KEY(trading_accounts_id) REFERENCES trading_accounts(Id_trading_accounts),
           PRIMARY KEY (stocks_ticker, trading_accounts_id)
		   )
          ''')
