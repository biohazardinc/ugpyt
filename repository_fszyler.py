# -*- coding: utf-8 -*-

import sqlite3
from datetime import datetime

# do zabawy statystyka
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt

#
# Ścieżka połączenia z bazą danych
#
db_path = 'trading_accounts.db'

#
# Wyjątek używany w repozytorium
#
class RepositoryException(Exception):
	def __init__(self, message, *errors):
		Exception.__init__(self, message)
		self.errors = errors

#
# Model danych
#

class TradingAccount():
	"""Model pojedynczego konta tradingowego
	"""
	def __init__(self, id_trading_accounts, opening_date=datetime.now(), active='active', closing_date='NULL', stocks=[]):
		self.id_trading_accounts = id_trading_accounts
		self.opening_date = opening_date
		self.active = active
		self.closing_date = closing_date
		self.stocks = stocks
		self.balance = sum([stock.stocks_qty * stock.buying_price for stock in self.stocks])

	def __repr__(self):
		return "<TradingAccount(id_trading_accounts='%s', account_opening_date='%s', account_active='%s', account_closing_date='%s', account_balance='%s', stocks='%s')>" % (
					self.id_trading_accounts, self.opening_date, self.active, self.closing_date, str(self.balance), str(self.stocks)
				)

class Stock():
	"""Model papierów wartościowych na koncie. Występuje tylko wewnątrz obiektu TradingAccount.
	"""
	def __init__(self, stocks_ticker, stocks_qty, buying_price):
		self.stocks_ticker = stocks_ticker
		self.stocks_qty = stocks_qty
		self.buying_price = buying_price

	def __repr__(self):
		return "<Stocks(stocks_ticker='%s', stocks_qty='%s', buying_price='%s')>" % (
					self.stocks_ticker, str(self.stocks_qty), str(self.buying_price)
				)				

#
# Klasa bazowa repozytorium
#
class Repository():
	def __init__(self):
		try:
			self.conn = self.get_connection()
		except Exception as e:
			raise RepositoryException('GET CONNECTION:', *e.args)
		self._complete = False

	# wejście do with ... as ...
	def __enter__(self):
		return self

	# wyjście z with ... as ...
	def __exit__(self, type_, value, traceback):
		self.close()

	def complete(self):
		self._complete = True

	def get_connection(self):
		return sqlite3.connect(db_path)

	def close(self):
		if self.conn:
			try:
				if self._complete:
					self.conn.commit()
				else:
					self.conn.rollback()
			except Exception as e:
				raise RepositoryException(*e.args)
			finally:
				try:
					self.conn.close()
				except Exception as e:
					raise RepositoryException(*e.args)

#
# repozytorium obiektow typu TradingAccount
#
class TradingAccountRepository(Repository):

	def add(self, trading_account):
		"""Metoda dodaje pojedyncze konto do bazy danych,
		wraz z przypisanymi do niego papierami wartościowymi.
		"""
		try:
			c = self.conn.cursor()
			# zapisz konto tradingowe
			balance = sum([stock.stocks_qty * stock.buying_price for stock in trading_account.stocks])
			c.execute('INSERT INTO trading_accounts (id_trading_accounts, opening_date, active, closing_date, balance) VALUES(?, ?, ?, ?, ?)',
						(trading_account.id_trading_accounts, trading_account.opening_date, trading_account.active, trading_account.closing_date, trading_account.balance)
					)
			# zapisz papiery na koncie
			if trading_account.stocks:
				for stock in trading_account.stocks:
					try:
						c.execute('INSERT INTO stocks (trading_accounts_id, stocks_ticker, stocks_qty, buying_price) VALUES(?, ?, ?, ?)',
										(trading_account.id_trading_accounts, stock.stocks_ticker, stock.stocks_qty, stock.buying_price)
								)
					except Exception as e:
						#print "stock add error:", e
						raise RepositoryException('error adding stock: %s, to trading account: %s' %
													(str(stock), str(trading_accounts.id_trading_accounts))
												)
		except Exception as e:
			#print "invoice add error:", e
			raise RepositoryException('error adding trading account %s' % str(trading_account))

	def delete(self, trading_account):
		"""Metoda usuwa pojedyncze konto z bazy danych,
		wraz ze wszystkimi papierami wartosciowymi do niej przypisanymi.
		"""
		try:
			c = self.conn.cursor()
			# usuń papier
			c.execute('DELETE FROM stocks WHERE trading_accounts_id=?', (trading_account.id_trading_accounts,))
			# usuń nagłowek
			c.execute('DELETE FROM trading_accounts WHERE id_trading_accounts=?', (trading_account.id_trading_accounts,))

		except Exception as e:
			#print "trading account delete error:", e
			raise RepositoryException('error deleting trading account %s' % str(trading_account))

	def getById(self, id_trading_accounts):
		"""Get tading account by id
		"""
		try:
			c = self.conn.cursor()
			c.execute("SELECT * FROM trading_accounts WHERE id_trading_accounts=?", (id_trading_accounts,))
			tra_row = c.fetchone()
			trading_account = TradingAccount(id_trading_accounts=id_trading_accounts)
			if tra_row == None:
				trading_account=None
			else:
				trading_account.opening_date = tra_row[1]
				trading_account.active = tra_row[2]
				trading_account.closing_date = tra_row[3]
				trading_account.balance = tra_row[4]
				c.execute("SELECT * FROM stocks WHERE trading_accounts_id=? order by stocks_ticker", (id_trading_accounts,))
				tra_stocks_rows = c.fetchall()
				stocks_list = []
				for stock_row in tra_stocks_rows:
					stock = Stock(stocks_ticker=stock_row[1], stocks_qty=stock_row[2], buying_price=stock_row[3])
					stocks_list.append(stock)
				trading_account.stocks=stocks_list
		except Exception as e:
			#print "trading account getById error:", e
			raise RepositoryException('error getting by id id_trading_accounts: %s' % str(id_trading_accounts))
		return trading_account

	def update(self, trading_account):
		"""Metoda uaktualnia pojedyncze konto w bazie danych,
		wraz ze wszystkimi papierami wartościowymi do niej przypisanymi.
		"""
		try:
			# pobierz z bazy trading account
			tra_oryg = self.getById(trading_account.id_trading_accounts)
			if tra_oryg != None:
				# konto jest w bazie: usuń je
				self.delete(trading_account)
			self.add(trading_account)

		except Exception as e:
			#print "trading account update error:", e
			raise RepositoryException('error updating trading account %s' % str(trading_account))



if __name__ == '__main__':

	# stworzenie konta
	try:
		with TradingAccountRepository() as trading_account_repository:
			trading_account_repository.add(
				TradingAccount(id_trading_accounts = 1, opening_date = datetime.now(), active='active', closing_date='NULL',
						stocks = [
							Stock(stocks_ticker = "ACW:US", stocks_qty = 500, buying_price = 12.45),
							Stock(stocks_ticker = "D5I:GR", stocks_qty = 50, buying_price = 125),
						]
					)
				)
			trading_account_repository.complete()
	except RepositoryException as e:
		print(e)

	print TradingAccountRepository().getById(1)
	
	# update konta
	try:
		with TradingAccountRepository() as trading_account_repository:
			trading_account_repository.update(
				TradingAccount(id_trading_accounts = 1,
						stocks = [
							Stock(stocks_ticker = "ACW:US", stocks_qty = 500, buying_price = 12.45),
							Stock(stocks_ticker = "D5I:GR", stocks_qty = 50, buying_price = 1250000),
							Stock(stocks_ticker = "GOOG:US", stocks_qty = 5234, buying_price = 530),
						]
					)
				)
			trading_account_repository.complete()
	except RepositoryException as e:
		print(e)
	
	# wypisanie zawartosci konta
	print TradingAccountRepository().getById(1)

	# usuniecie konta
	try:
		with TradingAccountRepository() as trading_account_repository:
			trading_account_repository.delete( TradingAccount(id_trading_accounts = 1) )
			trading_account_repository.complete()
	except RepositoryException as e:
		print(e)
		
	# dla tego typu bazy obliczenia statystyczne nie mają za bardzo sensu, ale zróbmy to, żeby pokazać, że możemy ;)
	# dodajmy najpierw konto z paroma roznymi walorami
	try:
		with TradingAccountRepository() as trading_account_repository:
			trading_account_repository.add(
				TradingAccount(id_trading_accounts = 1, opening_date = datetime.now(), active='active', closing_date='NULL',
						stocks = [
							Stock(stocks_ticker = "ACW:US", stocks_qty = 500, buying_price = 12.45),
							Stock(stocks_ticker = "D5I:GR", stocks_qty = 50, buying_price = 125),
							Stock(stocks_ticker = "STAN:LN", stocks_qty = 35436, buying_price = 23.56),
							Stock(stocks_ticker = "GSK:LN", stocks_qty = 2355, buying_price = 78.68),
							Stock(stocks_ticker = "GOOG:US", stocks_qty = 324, buying_price = 8.20),
							Stock(stocks_ticker = "KGHM:PL", stocks_qty = 895, buying_price = 234.89),
							Stock(stocks_ticker = "GHB:US", stocks_qty = 435, buying_price = 45.3),
							Stock(stocks_ticker = "LOL:GR", stocks_qty = 5, buying_price = 98.4),
							Stock(stocks_ticker = "HERE:LN", stocks_qty = 436, buying_price = 74),
							Stock(stocks_ticker = "KOF:LN", stocks_qty = 25, buying_price = 5.05),
							Stock(stocks_ticker = "MOOT:US", stocks_qty = 34, buying_price = 16.43),
							Stock(stocks_ticker = "METH:PL", stocks_qty = 85, buying_price = 12.98),
							Stock(stocks_ticker = "GH:LN", stocks_qty = 43, buying_price = 48.68),
							Stock(stocks_ticker = "GG:US", stocks_qty = 334, buying_price = 8.23),
							Stock(stocks_ticker = "HM:PL", stocks_qty = 856, buying_price = 134.89),
							Stock(stocks_ticker = "HB:US", stocks_qty = 424, buying_price = 49.3),
							Stock(stocks_ticker = "LL:GR", stocks_qty = 433, buying_price = 98.78),
							Stock(stocks_ticker = "HEE:LN", stocks_qty = 434, buying_price = 92),
							Stock(stocks_ticker = "KF:LN", stocks_qty = 234, buying_price = 1.64),
							Stock(stocks_ticker = "MT:US", stocks_qty = 59, buying_price = 198.23),
							Stock(stocks_ticker = "MH:PL", stocks_qty = 35, buying_price = 49.53),
						]
					)
				)
			trading_account_repository.complete()
	except RepositoryException as e:
		print(e)

	# zapiszmy sobie ceny poszczegolnych walorow do listy
	stock_prices = []
	
	for i, stock in enumerate(TradingAccountRepository().getById(1).stocks):
		stock_prices.append(TradingAccountRepository().getById(1).stocks[i].buying_price)
	
	print "Wszystkie ceny akcji na koncie: ", stock_prices
	
	# generalnie mamy ceny różnych akcji, ale powiedzmy że jest sens się nimi statystycznie pobawić w imię nauki Pythona ;)
	print "Parametry statystyczne cen akcji na koncie"
	print "Srednia: ", np.mean(stock_prices)
	print "Odchylenie standardowe: ", np.std(stock_prices)
	print "Mediana: ", np.median(stock_prices)
	print "Wariancja: ", np.var(stock_prices)
	print "Najnizsza cena: ", np.amin(stock_prices)
	print "Najwyzsza cena: ", np.amax(stock_prices)
	print "Zakres cen (max - min): ", np.ptp(stock_prices)
	print "Kwantyl rzedu 95%: ", np.percentile(stock_prices, 95)
	print "Kwantyl rzedu 90%: ", np.percentile(stock_prices, 90)
	normalny = stats.normaltest(stock_prices)
	print "Wyniki testu na normalnosc rozkladu cen: ", normalny
	
	if normalny.pvalue >= 0.05:
		print "Rozklad cen jest rozkladem normalnym"
	else:
		print "Rozklad cen nie jest rozkladem normalnym"
	
	# narysujmy histogram cen
	print "Histogram cen pojawi sie w nowym oknie"
	plt.hist(stock_prices)
	plt.show()
	
	# sprzatamy po sobie
	try:
		with TradingAccountRepository() as trading_account_repository:
			trading_account_repository.delete( TradingAccount(id_trading_accounts = 1) )
			trading_account_repository.complete()
	except RepositoryException as e:
		print(e)
